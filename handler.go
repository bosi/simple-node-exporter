package main

import (
	"fmt"
	"net/http"

	"github.com/prometheus/client_golang/prometheus/promhttp"
)

func handleMetrics(w http.ResponseWriter, r *http.Request) {
	registry := NewRegistry()
	addFileMetrics(registry)

	promhttp.HandlerFor(registry, promhttp.HandlerOpts{}).ServeHTTP(w, r)
}

func handleStatus(w http.ResponseWriter, _ *http.Request) {
	fmt.Fprint(w, "ok")
}
