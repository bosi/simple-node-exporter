#!/bin/bash
set -eu

WORKDIR=$(dirname "${0}")
METRIC_NAME="apt_update_count"
OUTPUT_FILE="${WORKDIR}/../additional/apt_updates.json"

UPDATES=$(/usr/lib/update-notifier/apt-check 2>&1)
UPDATES_NORMAL=$(printf ${UPDATES} | cut -d";" -f1)
UPDATES_SECURITY=$(printf ${UPDATES} | cut -d";" -f2)

printf "{
    \"Name\": \"${METRIC_NAME}\",
    \"Type\": \"gauge\",
    \"Help\": \"number of updates of apt\",
    \"Labels\": [\"type\"],
    \"Metrics\": [
        {\"LabelValues\": [\"normal\"], \"Value\": ${UPDATES_NORMAL}},
        {\"LabelValues\": [\"security\"], \"Value\": ${UPDATES_SECURITY}}
    ]
}" >${OUTPUT_FILE}
