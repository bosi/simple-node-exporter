#!/bin/bash

WORKDIR=$(dirname "${0}")
OUTPUT_FILE_USAGE="${WORKDIR}/../additional/disk_usages.yaml"
OUTPUT_FILE_TOTAL="${WORKDIR}/../additional/disk_total.yaml"

printf "
name: node_disk_bytes_used
type: gauge
help: number of updates of apt
labels: [ mountpath ]
metrics:
" > $OUTPUT_FILE_USAGE

printf "
name: node_disk_bytes_total
type: gauge
help: number of updates of apt
labels: [ mountpath ]
metrics:
" > $OUTPUT_FILE_TOTAL

df -B1 | tail -n +2 | while read -r line; do
    path=$(echo $line | awk '{print $6}')
    total=$(echo $line | awk '{print $2}')
    usage=$(echo $line | awk '{print $3}')

    echo "    -   labelValues: [ \"$path\" ]" >> $OUTPUT_FILE_USAGE
    echo "        value: $usage" >> $OUTPUT_FILE_USAGE
    echo "    -   labelValues: [ \"$path\" ]" >> $OUTPUT_FILE_TOTAL
    echo "        value: $total" >> $OUTPUT_FILE_TOTAL
done
