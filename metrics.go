//nolint:lll
package main

import (
	"fmt"
	"io"
	"log"
	"os"
	"runtime"
	"strings"
	"time"

	"github.com/prometheus/client_golang/prometheus"
	"github.com/shirou/gopsutil/v4/cpu"
	"github.com/shirou/gopsutil/v4/disk"
	"github.com/shirou/gopsutil/v4/load"
	"github.com/shirou/gopsutil/v4/mem"
)

var gauges = map[string]*prometheus.GaugeVec{}

func init() {
	if !disableCPUMetrics {
		gauges["cpuUsageGauge"] = prometheus.NewGaugeVec(prometheus.GaugeOpts{Namespace: "node", Subsystem: "cpu", Name: "usage", Help: "cpu usage per core"}, []string{"hostname", "core"})
		gauges["cpuCores"] = prometheus.NewGaugeVec(prometheus.GaugeOpts{Namespace: "node", Subsystem: "cpu", Name: "cores_total", Help: "total number of cpu cores"}, []string{"hostname"})

		if runtime.GOOS != "windows" {
			gauges["load1m"] = prometheus.NewGaugeVec(prometheus.GaugeOpts{Namespace: "node", Subsystem: "", Name: "load1", Help: "cpu load 1 minute"}, []string{"hostname"})
			gauges["load5m"] = prometheus.NewGaugeVec(prometheus.GaugeOpts{Namespace: "node", Subsystem: "", Name: "load5", Help: "cpu load 5 minutes"}, []string{"hostname"})
			gauges["load15m"] = prometheus.NewGaugeVec(prometheus.GaugeOpts{Namespace: "node", Subsystem: "", Name: "load15", Help: "cpu load 15 minutes"}, []string{"hostname"})
		}
	}

	if !disableDiskMetrics {
		gauges["diskTotalGauge"] = prometheus.NewGaugeVec(prometheus.GaugeOpts{Namespace: "node", Subsystem: "disk", Name: "bytes_total", Help: "total bytes of disk space"}, []string{"hostname", "mountpath"})
		gauges["diskUsedGauge"] = prometheus.NewGaugeVec(prometheus.GaugeOpts{Namespace: "node", Subsystem: "disk", Name: "bytes_used", Help: "used bytes of disk space"}, []string{"hostname", "mountpath"})
	}

	if !disableMemoryMetrics {
		gauges["memoryTotalGauge"] = prometheus.NewGaugeVec(prometheus.GaugeOpts{Namespace: "node", Subsystem: "memory", Name: "bytes_total", Help: "total bytes of memory"}, []string{"hostname"})
		gauges["memoryUsedGauge"] = prometheus.NewGaugeVec(prometheus.GaugeOpts{Namespace: "node", Subsystem: "memory", Name: "bytes_used", Help: "used bytes of memory"}, []string{"hostname"})
	}
}

// NewRegistry returns a new prometheus registry
func NewRegistry() *prometheus.Registry {
	r := prometheus.NewRegistry()

	for _, gauge := range gauges {
		r.MustRegister(gauge)
	}

	return r
}

func startUpdater() {
	name := hostname()
	log.Printf("hostname is \"%s\"\n", name)

	log.Println("start updater")
	for {
		update(name)

		time.Sleep(updateInterval)
	}
}

func hostname() string {
	if _, err := os.Stat("hostname"); err == nil {
		f, err := os.Open("hostname")
		if err == nil {
			c, err := io.ReadAll(f)
			if err == nil {
				return strings.TrimSpace(string(c))
			}
		}
	}

	h := os.Getenv("HOSTNAME")
	if h != "" {
		return h
	}

	if h == "" {
		h, _ = os.Hostname()
	}

	return h
}

func update(hostname string) {
	if !disableCPUMetrics {
		updateCPUMetrics(hostname)
	}

	if !disableMemoryMetrics {
		updateRAMMetrics(hostname)
	}

	if !disableDiskMetrics {
		updateDiskMetrics(hostname)
	}
}

func updateCPUMetrics(hostname string) {
	usages, err := cpu.Percent(1*time.Second, true)
	if err == nil {
		cs, err := cpu.Times(true)
		if err == nil {
			for i, c := range cs {
				gauges["cpuUsageGauge"].WithLabelValues(hostname, c.CPU).Set(usages[i])
			}
		} else {
			log.Printf("error while fetching cpu info: %s", err.Error())
		}
	} else {
		log.Printf("error while fetching cpu usage info: %s", err.Error())
	}

	if runtime.GOOS != "windows" {
		l, err := load.Avg()
		if err == nil {
			gauges["load1m"].WithLabelValues(hostname).Set(l.Load1)
			gauges["load5m"].WithLabelValues(hostname).Set(l.Load5)
			gauges["load15m"].WithLabelValues(hostname).Set(l.Load15)
		} else {
			logError(fmt.Errorf("error while fetching load info: %s", err.Error()))
		}
	}

	cores, err := cpu.Counts(true)
	if err == nil {
		gauges["cpuCores"].WithLabelValues(hostname).Set(float64(cores))
	} else {
		logError(fmt.Errorf("error while fetching cpu cores: %s", err.Error()))
	}
}

func updateRAMMetrics(hostname string) {
	v, err := mem.VirtualMemory()
	if err == nil {
		gauges["memoryTotalGauge"].WithLabelValues(hostname).Set(float64(v.Total))
		gauges["memoryUsedGauge"].WithLabelValues(hostname).Set(float64(v.Used))
	} else {
		logError(fmt.Errorf("error while fetching memory info: %s", err.Error()))
	}
}

func updateDiskMetrics(hostname string) {
	for _, p := range diskPaths() {
		us, err := disk.Usage(p)
		if err == nil {
			gauges["diskTotalGauge"].WithLabelValues(hostname, p).Set(float64(us.Total))
			gauges["diskUsedGauge"].WithLabelValues(hostname, p).Set(float64(us.Used))
		} else {
			logError(fmt.Errorf("error while fetching disk info: %s", err.Error()))
		}
	}
}

func diskPaths() []string {
	paths := []string{}
	if p := os.Getenv("DISK_PATH"); p != "" {
		paths = append(paths, p)
	}

	if p := os.Getenv("DISK_PATHS"); p != "" {
		paths = append(paths, strings.Split(p, ",")...)
	}

	if len(paths) == 0 {
		return []string{"/"}
	}

	return paths
}
