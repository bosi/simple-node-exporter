package main

import (
	"flag"
	"fmt"
	"io"
	"log"
	"net/http"
	"net/http/httptest"
	"os"
	"strings"
	"time"

	"github.com/prometheus/client_golang/prometheus/promhttp"
)

const (
	flagValueTrue = "true"
)

var (
	suppressErrors       bool
	disableCPUMetrics    = false
	disableMemoryMetrics = false
	disableDiskMetrics   = false
	updateInterval       = 10 * time.Second
)

func main() {
	printOnly := flag.Bool(
		"print-only",
		false,
		"if set metrics will printed out to stdout once instead of starting a webserver",
	)
	flag.BoolVar(&suppressErrors,
		"suppress-errors",
		false,
		"if set no errors will printed out",
	)
	flag.Parse()

	if v := os.Getenv("DISABLE_CPU_METRICS"); strings.ToLower(v) == flagValueTrue {
		log.Println("cpu metrics are disabled")
		disableCPUMetrics = true
	}

	if v := os.Getenv("DISABLE_MEMORY_METRICS"); strings.ToLower(v) == flagValueTrue {
		log.Println("memory metrics are disabled")
		disableMemoryMetrics = true
	}

	if v := os.Getenv("DISABLE_DISK_METRICS"); strings.ToLower(v) == flagValueTrue {
		log.Println("disk metrics are disabled")
		disableDiskMetrics = true
	}

	if v := os.Getenv("UPDATE_INTERVAL"); v != "" {
		d, err := time.ParseDuration(v)
		if err != nil {
			log.Fatalf("invalid UPDATE_INTERVAL: %s", err.Error())
		}
		updateInterval = d
	}

	log.Printf("update interval is %s", updateInterval.String())

	if *printOnly {
		fmt.Print(handleSingleExecution())
		return
	}

	go startUpdater()

	http.HandleFunc("/metrics", handleMetrics)
	http.HandleFunc("/status", handleStatus)

	log.Println("start http server")
	log.Fatal(http.ListenAndServe(":3322", nil))
}

func handleSingleExecution() string {
	update(hostname())

	req, _ := http.NewRequest(http.MethodGet, "/", nil)
	resp := httptest.NewRecorder()
	promhttp.HandlerFor(NewRegistry(), promhttp.HandlerOpts{}).ServeHTTP(resp, req)

	data, err := io.ReadAll(resp.Body)
	if err != nil {
		panic(err)
	}

	return string(data)
}
