package main

import (
	"encoding/json"
	"fmt"
	"log"
	"os"
	"strings"

	"gopkg.in/yaml.v3"

	"github.com/prometheus/client_golang/prometheus"
)

type (
	additionalMetric struct {
		Name        string       `json:"name"    yaml:"name"`
		Help        string       `json:"help"    yaml:"help"`
		Type        string       `json:"type"    yaml:"type"`
		Labels      []string     `json:"labels"  yaml:"labels"`
		JSONMetrics []jsonMetric `json:"metrics" yaml:"metrics"`
	}

	jsonMetric struct {
		LabelValues []string `json:"labelValues" yaml:"labelValues"`
		Value       float64  `json:"value"       yaml:"value"`
	}
)

func addFileMetrics(registry *prometheus.Registry) {
	dir, err := os.Stat("additional")
	if err != nil {
		log.Print(err.Error())
		return
	}

	fileInfos, err := os.ReadDir(dir.Name())
	if err != nil {
		logError(err)
		return
	}

	for _, fileInfo := range fileInfos {
		if strings.HasPrefix(fileInfo.Name(), ".") {
			continue
		}

		am, err := getMetricFromFile(dir, fileInfo)
		if err != nil {
			logError(err)
			continue
		}

		switch am.Type {
		case "gauge":
			labels := append(am.Labels, "hostname")
			m := prometheus.NewGaugeVec(
				prometheus.GaugeOpts{Namespace: "node", Subsystem: "", Name: am.Name, Help: am.Help},
				labels,
			)
			registry.MustRegister(m)
			for _, jm := range am.JSONMetrics {
				labelValues := append(jm.LabelValues, hostname())
				m.WithLabelValues(labelValues...).Set(jm.Value)
			}
		}
	}
}

func getMetricFromFile(dir os.FileInfo, fileInfo os.DirEntry) (additionalMetric, error) {
	am := additionalMetric{}

	file, err := os.Open(dir.Name() + "/" + fileInfo.Name())
	if err != nil {
		return am, err
	}

	var errJSON, errYAML error
	if errJSON = json.NewDecoder(file).Decode(&am); errJSON == nil {
		return am, nil
	}

	if _, err := file.Seek(0, 0); err != nil {
		return am, fmt.Errorf("failed to reset file pointer %q: %s", fileInfo.Name(), err.Error())
	}

	if errYAML = yaml.NewDecoder(file).Decode(&am); errYAML == nil {
		return am, nil
	}

	return am, fmt.Errorf(
		"failed to decode %q as json (%s) or yaml (%s)",
		fileInfo.Name(),
		errJSON.Error(),
		errYAML.Error(),
	)
}
