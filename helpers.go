package main

import "log"

func logError(err error) {
	if suppressErrors {
		return
	}

	log.Printf("| ERROR | %s \n", err.Error())
}
