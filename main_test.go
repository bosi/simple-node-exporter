package main

import (
	"testing"

	"github.com/stretchr/testify/require"
)

func TestHandleSingleExecution(t *testing.T) {
	t.Parallel()

	ret := handleSingleExecution()
	require.Contains(t, ret, hostname())
}
