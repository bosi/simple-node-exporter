package main

import (
	"fmt"
	"os"
	"testing"

	"github.com/stretchr/testify/require"

	ioprometheusclient "github.com/prometheus/client_model/go"
	"syreclabs.com/go/faker"
)

//nolint:paralleltest
func TestUpdate(t *testing.T) {
	host := faker.Lorem().Word()
	update(host)

	m := &ioprometheusclient.Metric{}

	for name, gauge := range gauges {
		t.Run("check: "+name, func(t *testing.T) {
			if name != "cpuUsageGauge" && name != "diskTotalGauge" && name != "diskUsedGauge" {
				require.Nil(t, gauge.WithLabelValues(host).Write(m))
				require.True(t, m.GetGauge().GetValue() > float64(0), "check that gauge is greater then 0")
			}
		})
	}
}

func TestNewRegistry(t *testing.T) {
	t.Parallel()

	r := NewRegistry()

	mf, err := r.Gather()
	require.Nil(t, err)

	require.Len(t, mf, len(gauges))
}

//nolint:paralleltest
func TestHostname(t *testing.T) {
	os.Remove("hostname")

	t.Run("check hostname from system", func(t *testing.T) {
		t.Setenv("HOSTNAME", "")
		hn, err := os.Hostname()
		require.Nil(t, err)
		require.Equal(t, hostname(), hn)
	})

	t.Run("check hostname from file", func(t *testing.T) {
		newHn := faker.Lorem().Word()
		t.Setenv("HOSTNAME", newHn)
		require.Equal(t, hostname(), newHn)
	})

	t.Run("check hostname from file", func(t *testing.T) {
		var err error
		f, err := os.Create("hostname")
		require.Nil(t, err)
		newHn := faker.Lorem().Word()
		_, err = fmt.Fprint(f, newHn)
		require.Nil(t, err)
		require.Equal(t, hostname(), newHn)
	})
}

//nolint:paralleltest
func TestDiskPaths(t *testing.T) {
	require.Nil(t, os.Setenv("DISK_PATH", ""))
	require.Equal(t, []string{"/"}, diskPaths())

	require.Nil(t, os.Setenv("DISK_PATH", "/something/special"))
	require.Equal(t, []string{"/something/special"}, diskPaths())

	require.Nil(t, os.Setenv("DISK_PATHS", "/,/hello/world"))
	require.Equal(t, []string{"/something/special", "/", "/hello/world"}, diskPaths())
}
