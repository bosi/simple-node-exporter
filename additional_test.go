package main

import (
	"fmt"
	"net/http"
	"net/http/httptest"
	"os"
	"testing"

	"github.com/stretchr/testify/require"

	"syreclabs.com/go/faker"
)

//nolint:paralleltest
func TestAddFileMetrics(t *testing.T) {
	m1 := faker.Lorem().Word()
	m2 := faker.Lorem().Word()
	m3 := faker.Lorem().Word()

	f1, err := os.Create("additional/testfile_1")
	require.Nil(t, err)
	f2, err := os.Create("additional/testfile_2")
	require.Nil(t, err)
	f3, err := os.Create("additional/testfile_3")
	require.Nil(t, err)

	_, err = fmt.Fprintf(f1, `{"Name":"%s", "Type": "gauge", "Metrics":[{}]}`, m1)
	require.Nil(t, err)
	_, err = fmt.Fprintf(f2, `{"Name":"%s", "Type": "gauge", "Metrics":[{}]}`, m2)
	require.Nil(t, err)
	_, err = fmt.Fprintf(f3, "name: %s\ntype: gauge\nmetrics: [ { } ]", m3)
	require.Nil(t, err)

	req, err := http.NewRequest(http.MethodGet, "/", nil)
	require.Nil(t, err)

	t.Run("check with existing additional/ dir", func(t *testing.T) {
		rr := httptest.NewRecorder()
		handleMetrics(rr, req)

		require.Contains(t, rr.Body.String(), m1)
		require.Contains(t, rr.Body.String(), m2)
		require.Contains(t, rr.Body.String(), m3)
	})

	t.Run("check with missing additional/ dir", func(t *testing.T) {
		require.Nil(t, os.Rename("additional", "additional-gone"))

		rr := httptest.NewRecorder()
		handleMetrics(rr, req)

		require.NotContains(t, rr.Body.String(), m1)
		require.NotContains(t, rr.Body.String(), m2)
		require.NotContains(t, rr.Body.String(), m3)

		require.Nil(t, os.Rename("additional-gone", "additional"))
	})
}
