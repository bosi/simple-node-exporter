<img src="https://gitlab.com/bosi/simple-node-exporter/raw/master/logo.png" width="400" alt="Logo">

# Simple Node Exporter

[![pipeline status](https://gitlab.com/bosi/simple-node-exporter/badges/master/pipeline.svg)](https://gitlab.com/bosi/simple-node-exporter/-/commits/master)
[![coverage report](https://gitlab.com/bosi/simple-node-exporter/badges/master/coverage.svg)](https://gitlab.com/bosi/simple-node-exporter/-/commits/master)

A simple node exporter for prometheus metrics. It provides a subset
of [https://github.com/prometheus/node_exporter](https://github.com/prometheus/node_exporter) but smaller and without
the necessity to mount the whole filesystem into the container.

## General facts

### Why I start this project?

If you found this repo you will properly familiar with prometheus and
the [node exporter](https://github.com/prometheus/node_exporter) which is an official tool to grab a lot of metrics from
a server/node. My problem was that the original node exporter needs access to the whole file system and I want to have a
tool with minimized dependencies, which can run on any linux server with installed docker and no requirement to mount
any files from the host system into the docker container.

### What was the goal of the project?

Create a simple tool which can be used at every of my linux servers with ease. I use prometheus and grafana to monitor
them so a `/metrics`-endpoint for prometheus is the only thing I need.

### What exactly does the tool?

This tool uses [gopsutil](https://github.com/shirou/gopsutil) and
the [prometheus golang client](https://github.com/prometheus/client_golang) to fetch basic information about cpu, memory
and disk and provide them as prometheus metrics.

## Features

### Webserver

The simple-node-exporter only has one endpoint `/metrics` and provides the following metrics:

| metric                  | description                                    |
|-------------------------|------------------------------------------------|
| node_cpu_cores_total    | number of  cpu cores                           |
| node_cpu_usage          | cpu usage per core                             |
| node_disk_bytes_total   | total bytes of disk space                      |
| node_disk_bytes_used    | used bytes of disk space                       |
| node_load1              | cpu load 1 minute (not available on windows)   |
| node_load5              | cpu load 5 minute (not available on windows)   |
| node_load15             | cpu load 15 minute  (not available on windows) |
| node_memory_bytes_total | total bytes of memory                          |
| node_memory_bytes_used  | used bytes of memory                           |

You can also add any other metric. Have a look at the following example to get an idea how the configuration should look
like. The json or yaml must be placed inside the directory `additional/`

Example-file:

```json
{
    "name": "node_apt_update_count",
    "type": "gauge",
    "help": "number of updates of apt",
    "labels": ["type"],
    "metrics": [
        {
            "labelValues": ["normal"],
            "value": 5
        },
        {
            "labelValues": ["security"],
            "value": 0
        }
    ]
}
```

```yaml
name: node_apt_update_count
type: gauge
help: number of updates of apt
labels: [ type ]
metrics:
    -   labelValues: [ normal ]
        value: 5
    -   labelValues: [ security ]
        value: 0
```

This json/yaml will create the following metrics:

```
# HELP node_node_apt_update_count number of updates of apt
# TYPE node_node_apt_update_count gauge
node_apt_update_count{hostname="myhost",type="normal"} 5
node_apt_update_count{hostname="myhost",type="security"} 0
```

Please also have a look at the examples for generating metric-files via script.

### Single Execution

If you don't like or don't need a webserver to run on your server you can use the flag `-print-only`.

```
./simple-node-exporter -print-only
```

This will print out the data which is normally provided via `/metrics` endpoint to stdout. You can pipe the output into
any file to e.g. use the data in other scripts or provide the data by a webserver which is already running on your
server. This can be useful on managed servers/webhosters where you are not able to install software or block additional
ports.

If you do not want any error to be printed you can use the flag `-suppress-errors` when starting the program.

#### Example

Let's assume you have a managed server at hetzner (a german server and cloud provider). Since the server is managed by
hetzner you cannot install software by yourself or block additional ports. Nevertheless, an apache webserver is already
running on the system. Since we don't want outdated data we use the system cron and add the following line:

```
* * * * * /path/to/simple-node-exporter -print-only > /path/to/web/root/metrics
```

You should also setup a protection via htaccess to prevent exposing sensitive data to public.

Important: there might be constraints which prevent this example to work in your special case.

## Usage

You can simply use the following `docker-compose.yml` to test and use the simple-node-exporter

```yaml
version: "3"

services:
    simple-node-exporter:
        image: bosix/simple-node-exporter
        ports:
            - "3322:3322"
        environment:
            - "HOSTNAME=myhost"
        volumes:
            - ./additional:/app/additional
```

```
docker-compose up -d
```

After starting the container you can call the metrics with `curl http://localhost:3322/metrics`. The
environment-variable `hostname` can be used to identify the server. It will be added as a label to all metrics.

If you don't trust me, the docker-image or the binary I put into the image, you can of course compile and build
everything by yourself:

```bash
# build the standalone binary (requires go1.16+)
go build .

# build the docker image (requires docker or any other image creating tool) 
docker build -t simple-node-exporter .
```

### Configuration

Config can be set via env:

| name                     | description                                                                                                                | default          |
|--------------------------|----------------------------------------------------------------------------------------------------------------------------|------------------|
| `HOSTNAME`               | name to identify the machine; will be added to all metrics as label                                                        | systems hostname |
| `DISK_PATH`              | (deprecated) mounting path of disk which should be measured by default; mounting paths can be found on linux e.g. via `df` | `/`              |
| `DISK_PATHS`             | comma separated list of mounting paths                                                                                     | empty            |
| `DISABLE_CPU_METRICS`    | disable cpu load and usage metrics                                                                                         | `false`          |
| `DISABLE_MEMORY_METRICS` | disable memory total and usage metrics                                                                                     | `false`          |
| `DISABLE_DISK_METRICS`   | disable disk total and usage metrics                                                                                       | `false`          |
| `UPDATE_INTERVAL`        | the interval for updating metrics internally                                                                               | `10s`            |

The hostname can also be set by mounting a file named `hostname` next to the executable. Inside the provided docker
image it would be `/app/hostname`.

## Limitations

Please take notice that in some cases some metrics might not work. A known issue is e.g. that on windows load1/5/15 is
always 0.

## Used 3rd Party Libraries

This app uses several 3rd party libraries. Their licenses can be
viewed [here](https://bosi.gitlab.io/simple-node-exporter/licenses-3rd-party.txt) (gitlab pages).
